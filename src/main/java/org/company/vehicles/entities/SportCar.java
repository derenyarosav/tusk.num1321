package com.company.vehicles.entities;

public class SportCar extends Car{
    private int MaximumSpeed;

    public int getMaximumSpeed() {
        return MaximumSpeed;
    }

    public void setMaximumSpeed(int maximumSpeed) {
        MaximumSpeed = maximumSpeed;
    }

    public SportCar(String CR, String CC, int w) {
        super(CR, CC, w);
    }
}
