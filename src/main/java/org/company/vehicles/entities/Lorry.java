package com.company.vehicles.entities;

public class Lorry extends Car{

    private int CarryingCapacityOfTheBody;

    public int getCarryingCapacityOfTheBody() {
        return CarryingCapacityOfTheBody;
    }

    public void setCarryingCapacityOfTheBody(int carryingCapacityOfTheBody) {
        CarryingCapacityOfTheBody = carryingCapacityOfTheBody;
    }

    public Lorry(String CR, String CC, int w) {
        super(CR, CC, w);
    }
}
